# Ultimate Homeserver - Eierlegende Wollmilchsau

In german we call this one thing that can do everything something like the Egg laying, milk giving, wooly sow. This is a collection of scripts, configs and ideas to create something close to being the perfect homeserver.

## Ideas




## Basic system installation

I will use Ubuntu as a base for my homeserver for some simple reasons. I'm used to Ubuntu as a server system from daily business and there is a lot of software available prebuilt for Ubuntu. It is easy to install and quite up2date.


There is some basic stuff that I want on my systems

```
sudo apt install vim htop 

```

## Basic GUI with chromium running a family dashboard
I know it is a server and there shouldn't be anything like a Desktop on it. Anyways it is for home use and it just runs a fullscreen browser showing a webpage. 
Alternatively you can skip this section and display the stuff from a Raspberry Pi or similar.
On my basic Ubuntu Server setup I will add the Xubuntu Core desktop that should only include the minimal stuff. 
Additinally we will install chromium for displaying a webpage, in this case it is a [dakboard.com](dakboard.com) dashboard displaying some family calendars and photos. 
We will also add a dedicated user to the system that only runs the stuff on the desktop and has no further permissions.

Install the packages for Xubuntu and Chromium

```
sudo apt install xubuntu-core chromium-browser
```

Add a user called display to login to XFCE
```
sudo adduser display
```

